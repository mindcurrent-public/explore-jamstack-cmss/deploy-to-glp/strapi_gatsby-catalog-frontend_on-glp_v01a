# strapi_gatsby-catalog-frontend_on-GLP_v01a

We see if we can deploy the Gatsby front-end of Strapi's "Catalog Starter" onto GitLab Pages.

#### Approach:
* We will use our local version of GitLab's "Gatsby Starter" project to create a Gatsby instance running on GLP (GitLab Pages).
  * Here is our GitLab project: [GLP-gatsby-starter_v01a](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/deploy-to-glp/glp-gatsby-starter_v01a) which will serve as a destination scaffold.  
* Then we'll try to smash (errr... I mean "merge") the Strapi Catalog Starter App's `my-project\frontend` Gatsby structure into this scaffold.
  * Here is our GitLab project: [Strapi-Gatsby-Catalog_v01d](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/strapi-cms-ecosystem/strapi-gatsby-catalog_v01d) which will serve as a source scaffold. 
* As we proceed, we'll [track our progress on a per-issue basis](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/deploy-to-glp/strapi_gatsby-catalog-frontend_on-glp_v01a//-/issues), in a "GSD using micro-tasks" manner.
* Useful SOPs can be constructed from the project history tracked via the **[closed issues](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/deploy-to-glp/strapi_gatsby-catalog-frontend_on-glp_v01a//-/issues?scope=all&state=closed)**.

#### Results:

...Success.  Whenever we check in a change, the GitLab CI/CD pipeline kicks off, and we (re)build the static site...   
...which can be accessed at: https://mindcurrent-public.gitlab.io/explore-jamstack-cmss/deploy-to-glp/strapi_gatsby-catalog-frontend_on-glp_v01a


### Resources:

#### Dynalist Action-Items & Knowledge Bases:
* Strapi Knowledge Base ([via DynaList](https://dynalist.io/d/XoMkxmR1c7xCcPHNG4zUrWEi#z=7ujn_CDYtGZ-xMse1r3VqIKe))
* Gatsby Knowledge Base ([via DynaList](https://dynalist.io/d/XoMkxmR1c7xCcPHNG4zUrWEi#z=u-B_PD2Nlr0KahscXewpfW5E))
* `git` Knowledge Base ([via DynaList](https://dynalist.io/d/XoMkxmR1c7xCcPHNG4zUrWEi#z=NvCDWmSatn8N1dcJWouIOrIR))
