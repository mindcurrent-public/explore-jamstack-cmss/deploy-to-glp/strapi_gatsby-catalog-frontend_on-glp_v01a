# GLP-gatsby-starter_v01a

We will use GitLab's "Gatsby Starter" project to create a Gatsby instance running on GLP (GitLab Pages).
* Here is GitLab Page's default "Gatsby Starter" repo: https://gitlab.com/pages/gatsby
* (...which deploys to GLP's example Gatsby site using GitLab Pages: https://pages.gitlab.io/gatsby)

#### Approach:
* As we proceed, we'll [track our progress on a per-issue basis](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/deploy-to-glp/glp-gatsby-starter_v01a/-/issues), in a "GSD using micro-tasks" manner.
* Useful SOPs can be constructed from the project history tracked via the [closed issues](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/deploy-to-glp/glp-gatsby-starter_v01a/-/issues?scope=all&state=closed).

#### Results:

Whenever we check in a change, the GitLab CI/CD pipeline kicks off, and we (re)build the static site...   
...which can be accessed at: https://mindcurrent-public.gitlab.io/explore-jamstack-cmss/deploy-to-glp/glp-gatsby-starter_v01a
